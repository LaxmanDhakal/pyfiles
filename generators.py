def new_decorator(original_func):
    def wrap_func():
        print("some extracode before in original function")
        original_func()
        print('some code after original function ')
    return wrap_func


@new_decorator
def func_need_decorator():
    print("from decoration")


func_need_decorator()



